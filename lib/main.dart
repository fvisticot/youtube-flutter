import 'package:flutter/material.dart';
import 'youtube_service.dart';
import 'package:flutter/foundation.dart';
import 'video_detail.dart';
import 'package:flutter_search_bar/flutter_search_bar.dart';

void main() => runApp(new MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return new MaterialApp(
      theme: ThemeData.dark(),
      home: new MyHomePage(title: 'Youtube search'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => new _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  List<Video> _videos = new List<Video>();
  bool _isLoading = false;
  SearchBar _searchBar;

  _MyHomePageState() {
    _searchBar = new SearchBar(
      inBar: false,
      setState: setState,
      onSubmitted: onSubmitted,
      buildDefaultAppBar: buildAppBar,
    );
  }

  @override
  void initState() {
    super.initState();
    getVideos();
  }

  void onSubmitted(String value) {
    setState(() {
      getVideos(query: value);
    });
  }

  void getVideos({String query = 'surf'}) async {
    List<Video> videos = await new YoutubeService().getVideos(query: query);
    setState(() {
      _videos = videos;
    });
  }

  AppBar buildAppBar(BuildContext context) {
    return new AppBar(
        title: new Text(widget.title),
        actions: [_searchBar.getSearchAction(context)]);
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
        appBar: _searchBar.build(context),
        body: new Container(
            padding: new EdgeInsets.all(8.0),
            child: new Stack(children: <Widget>[
              _isLoading ? new CircularProgressIndicator() : new Container(),
              new ListView(
                children: ListTile.divideTiles(
                  context: context,
                  tiles: _buildVideosItems(),
                ).toList(),
              )
            ]))
        // This trailing comma makes auto-formatting nicer for build methods.
        );
  }

   _buildVideosItems() {
    return _videos.map((Video video) => _buildVideoItem(video));
  }

  Widget _buildVideoItem(Video video) {
    print(video);
    return new ListTile(
        leading: new Image.network(video.thumbnails[0].url),
        title: new Text(video.title),
        subtitle: new Text(video.description),
        onTap: () {
          Navigator.push(
            context,
            new MaterialPageRoute(
              builder: (_) => new VideoDetail(video: video),
            ),
          );
        });
  }
}
