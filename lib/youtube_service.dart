import 'dart:async';
import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

class YoutubeService {
  static final String KEY = 'AIzaSyADE8qoQ89vLnyGLdr_Ai8zPHVCbb_lqBI';

  Future<List<Video>> getVideos({String query, int maxResults = 25}) async {
    final response = await http.get(
        'https://www.googleapis.com/youtube/v3/search?key=${KEY}&maxResults=${maxResults}&q=${query}&part=snippet');
    final responseJson = json.decode(response.body);
    List items = responseJson['items'];

    List<Video> videos = items.map((item) {
      Video video = Video.fromJson(item);
      return video;
    }).toList();
    return videos;
  }
}

class Thumbnail {
  final String type;
  final int width;
  final int height;
  final String url;

  Thumbnail({this.type, this.width, this.height, this.url});

  @override
  String toString() {
    return 'Thumbnail {type: $type, width: $width, height: $height, url: $url}';
  }

  factory Thumbnail.fromJson(Map<String, dynamic> json) {
    return new Thumbnail(
        width: json['width'], height: json['height'], url: json['url']);
  }
}

class Video {
  final String videoId;
  final String title;
  final String description;
  final List<Thumbnail> thumbnails;

  Video({this.videoId, this.title, this.description, this.thumbnails});

  @override
  String toString() {
    return 'Video {id: $videoId, title: $title, description: $description, thumbnails: $thumbnails}';
  }

  factory Video.fromJson(Map<String, dynamic> json) {
    List<String> sizes = ['default', 'medium', 'high'];
    List<Thumbnail> thumbnails = sizes.map((size) {
      var thumbnailDic = json['snippet']['thumbnails'][size];
      return new Thumbnail(
          type: size,
          width: thumbnailDic['width'],
          height: thumbnailDic['height'],
          url: thumbnailDic['url']);
    }).toList();

    return new Video(
        videoId: json['id']['videoId'],
        title: json['snippet']['title'],
        description: json['snippet']['description'],
        thumbnails: thumbnails);
  }
}
