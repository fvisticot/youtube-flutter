import 'youtube_service.dart';
import 'package:flutter/material.dart';
import 'youtube_service.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter_youtube/flutter_youtube.dart';

class VideoDetail extends StatelessWidget {
  final Video video;

  VideoDetail({this.video});

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
        appBar: new AppBar(
          title: new Text(video.title),
        ),
        body: new Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisSize: MainAxisSize.max,
            children: <Widget>[
              new Container(
                  color: Colors.white,
                  height: 250.0,
                  child: new Stack(fit: StackFit.expand, children: <Widget>[
                    new Image.network(
                      video.thumbnails[1].url,
                      fit: BoxFit.fill,
                    ),
                    new IconButton(
                      icon: new Icon(
                        Icons.play_circle_outline,
                        size: 80.0,
                      ),
                      tooltip: 'Play',
                      onPressed: () {
                        playYoutubeVideo();
                      },
                    )
                  ])),
              new Padding(
                  padding: new EdgeInsets.all(10.0),
                  child: new Text(
                    video.title,
                    maxLines: 2,
                    overflow: TextOverflow.ellipsis,
                    style: new TextStyle(
                        fontSize: 18.0, fontWeight: FontWeight.bold),
                  )),
              new Padding(
                  padding: new EdgeInsets.all(10.0),
                  child: new Text(
                    video.description,
                    maxLines: 10,
                    overflow: TextOverflow.ellipsis,
                    style: new TextStyle(fontWeight: FontWeight.normal),
                  )),
            ])
        // This trailing comma makes auto-formatting nicer for build methods.
        );
  }

  void playYoutubeVideo() {
    print('playing video ${video.videoId}');

    try {
      FlutterYoutube.playYoutubeVideoById(
        apiKey: YoutubeService.KEY,
        videoId: video.videoId,
      );
    } catch (e) {
      print(e);
    }
  }
}
